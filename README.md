# Conversor de base

O programa acima realiza a converção de bases, no momento executa apena a conversão de bases decimais para binárias e hexadecimais , porém 
logo estaremos implementando outras conversões.

Para utilizar o programa basta executa-lo, escolher a conversão desejada, e digitar o número que deseja converter.

## Desenvolvedores


| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
<img src="https://gitlab.com/uploads/-/system/user/avatar/8279861/avatar.png?width=400" width = 100> | Eguinaldo Couras | Gui.oliveira | [eguinaldocouras@gmail.com](mailto:eguinaldocouras@gmail.com)
| <img src="https://gitlab.com/uploads/-/system/user/avatar/8279920/avatar.png?width=400" width = 100>  | Arthur Klipp | Hellraiser | [arthurklipp@alunos.utfpr.edu.br](mailto:arthurklipp@alunos.utfpr.edu.br)
| <img src="https://gitlab.com/uploads/-/system/user/avatar/8231107/avatar.png?width=400" width = 100>  | Julio V Perin| JulioPerin | [julioperin@live.com](mailto:julioperin@live.com)
<img src="https://gitlab.com/uploads/-/system/user/avatar/5669368/avatar.png?width=400" width = 100> | Samuel H Fabricio | SamuelFabricio | [samuel.1995@alunos.utfpr.edu.br](mailto:samuel.1995@alunos.utfpr.edu.br)

